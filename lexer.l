/********************************************************
 * ex1.l 
 ********************************************************/
%{
#include "node.h"
#include "parser.hpp"
#include <iostream>

extern int yyerror(const char *p);
%}

%option noyywrap
%option yylineno

%%

EOF                     { return 0; }
"//".*                  {}
"#".*                   {}
[0-9]+                  { yylval.str = new std::string(yytext); return INTEGER; }
"typedef"               { return TYPEDEF; }
"struct"                { return STRUCT; }
"u8_t"                  { return U8; }
"u16_t"                 { return U16; }
"u32_t"                 { return U32; }
"s8_t"                  { return S8; }
"s16_t"                 { return S16; }
"s32_t"                 { return S32; }
[a-zA-Z_][a-zA-Z_0-9]*  { yylval.str = new std::string(yytext); return IDENT; }
"{"                     { return LBRACE; }
"}"                     { return RBRACE; }
"["                     { return LSQUARE; }
"]"                     { return RSQUARE; }
";"                     { return SC; }
[ \t\n]+                { }
\"([^\\\n]|(\\.))*?\"   { return SCONST; }
.                       { yyerror("Unrecognized token!"); }
"/*"        {
    register int c;

    for ( ; ; )
    {
        while ( (c = yyinput()) != '*' &&
                c != EOF )
            ;    /* eat up text of comment */

        if ( c == '*' )
        {
            while ( (c = yyinput()) == '*' )
                ;
            if ( c == '/' )
                break;    /* found the end */
        }

        if ( c == EOF )
        {
            yyerror("EOF in comment");
            break;
        }
    }
}

%%
