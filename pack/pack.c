#include "pack.h"

bool pack_u8(stream_t *dst, u8_t src)
{
    if (dst->sz == 0)
    {
        return false;
    }

    dst->sz--;
    *dst->b++ = src;

    return true;
}

bool unpack_u8(u8_t *dst, stream_t *src)
{
    if (src->sz == 0)
    {
        return false;
    }

    src->sz--;
    *dst = *src->b++;
    return true;
}

bool pack_u16(stream_t *dst, u16_t src)
{
    bool ret = pack_u8(dst, (src >> 8) & 0xff);
    if (ret == true)
    {
        ret = pack_u8(dst, (src & 0xff));
    }

    return ret;
}

bool unpack_u16(u16_t *dst, stream_t *src)
{
    u8_t msb;
    u8_t lsb;

    bool ret = unpack_u8(&msb, src);
    if (ret == true)
    {
        ret = unpack_u8(&lsb, src);
    }

    if (ret == true)
    {
        *dst = ((u16_t)msb) << 8 | lsb;
    }

    return ret;
}

bool pack_u32(stream_t *dst, u32_t src)
{
    bool ret = pack_u16(dst, (src >> 16) & 0xffff);
    if (ret == true)
    {
        ret = pack_u16(dst, (src & 0xffff));
    }

    return ret;
}

bool unpack_u32(u32_t *dst, stream_t *src)
{
    u16_t msw;
    u16_t lsw;

    bool ret = unpack_u16(&msw, src);
    if (ret == true)
    {
        ret = unpack_u16(&lsw, src);
    }

    if (ret == true)
    {
        *dst = ((u32_t)msw) << 16 | lsw;
    }

    return ret;
}
