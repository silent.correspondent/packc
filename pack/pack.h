#ifndef PACK_H
#define PACK_H

#include "types.h"

typedef struct stream_s
{
    char *b;
    size_t sz;
}
stream_t;

bool pack_u8(stream_t *dst, u8_t src);
bool unpack_u8(u8_t *dst, stream_t *src);

bool pack_u16(stream_t *dst, u16_t src);
bool unpack_u16(u16_t *dst, stream_t *src);

bool pack_u32(stream_t *dst, u32_t src);
bool unpack_u32(u32_t *dst, stream_t *src);

#endif
