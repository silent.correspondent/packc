/********************************************************
 * ex1.ypp 
 ********************************************************/
%{
#include <iostream>
#include <string>
#include <map>
#include <cstdlib> //-- I need this for atoi
#include <memory>
#include "node.h"
#include "packc.h"

//-- Lexer prototype required by bison, aka getNextToken()
int yylex(); 

extern int yylineno;
extern char *yytext;

int yyerror(const char *p)
{
    std::cerr << yylineno << ": " << p << " at " << yytext << std::endl;
}

static const char *source;
static const char *target;
%}

//-- SYMBOL SEMANTIC VALUES -----------------------------
%union {
  char sym;
  std::string *str;
  Typename *type_name;
  Field *field;
  Array *array;
  Node *node;
  Node_list *node_list;
  Struct *statement;
  Struct_list *statement_list;
};
%token <sym> LBRACE RBRACE LSQUARE RSQUARE SC
%token <sym> TYPEDEF STRUCT
%token <sym> U8 S8 U16 S16 U32 S32
%token <str> INTEGER IDENT SCONST

%type<field> field_member
%type<array> array_member
%type<node> member
%type<statement> statement
%type<node_list> member_list
%type<statement_list> statement_list program
%type<type_name> type_decl

//-- GRAMMAR RULES ---------------------------------------
%%

program: statement_list                     {
                                                generate_header(source, target, *$$);
                                                generate_source(target, *$$);
                                                delete $$;
                                            }
       ;

statement_list : statement_list statement   {
                                                Struct_ptr np = Struct_ptr($2); 
                                                $1->push_back(std::move(np));
                                                $$ = $1;
                                            }
               | statement                  {
                                                $$ = new Struct_list;
                                                Struct_ptr np = Struct_ptr($1); 
                                                $$->push_back(std::move(np));
                                            }
               ;

statement : TYPEDEF STRUCT LBRACE member_list RBRACE IDENT SC   {   
                                                                    $$ = new Struct($6, $4); 
                                                                }
          | TYPEDEF STRUCT IDENT LBRACE member_list RBRACE IDENT SC {
                                                                        $$ = new Struct($7, $5); 
                                                                    }
          ;

member_list : member_list member    {
                                        Node_ptr np = Node_ptr($2); 
                                        $1->push_back(std::move(np));
                                        $$ = $1;
                                    }
            ;

member_list : member    { 
                            $$ = new Node_list; 
                            Node_ptr np = Node_ptr($1); 
                            $$->push_back(std::move(np)); 
                        }
            ;

member : field_member   { $$ = $1; }
       | array_member   { $$ = $1; }
       ;

field_member : type_decl IDENT SC   { $$ = new Field($1, $2); }
             ;

array_member : type_decl IDENT LSQUARE INTEGER RSQUARE SC   { $$ = new Array($1, $2, $4); }
             | type_decl IDENT LSQUARE IDENT RSQUARE SC     { $$ = new Array($1, $2, $4); }
             ;

type_decl : U8      { $$ = new Typename(new std::string("u8"), false); }
          | S8      { $$ = new Typename(new std::string("s8"), false); }
          | U16     { $$ = new Typename(new std::string("u16"), false); }
          | S16     { $$ = new Typename(new std::string("s16"), false); }
          | U32     { $$ = new Typename(new std::string("u32"), false); }
          | S32     { $$ = new Typename(new std::string("s32"), false); }
          | IDENT   { $$ = new Typename($1, true); }
          ;
%%
//-- FUNCTION DEFINITIONS ---------------------------------
extern FILE *yyin;
int compile(const char *file, const char *output)
{
    source = file;
    target = output;
    int result = 0;

    if (file)
    {
        FILE *fp = fopen(file, "rt");
        if (fp == NULL)
        {
            std::cerr << "Couldn't open " << file<< std::endl;
            return -1;
        }
        yyin = fp;
        result = yyparse();
        fclose(fp);
    }
    else
    {
        result = yyparse();
    }
    return result;
}

