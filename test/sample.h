/* The user needs to supply the token values generated
   for each struct. A token is the struct name in
   uppercase prefixed with MSG_TOKEN_
*/
#ifndef SAMPLE_H
#define SAMPLE_H

#include "types.h"

#define SERIAL_LEN  7

// Definition of structs
typedef struct hello {
    u8_t serial[SERIAL_LEN];
    u16_t version;
}
hello;

typedef struct test32 {
    u32_t value;
}
test32;

typedef struct test16 {
    u16_t value;
}
test16;

typedef struct gapfill {
    u8_t    v8;
    u16_t   v16;
    u32_t   v32;
}
gapfill;

typedef struct funkyarrays {
    u8_t pad;
    u16_t a16[3];
    u32_t a32[2];
}
funkyarrays;

typedef struct unit_stats {
    u8_t dins;
    u8_t douts;
    u8_t ovf;
}
unit_stats;

typedef struct {
    unit_stats stats;
}
sub1;

typedef struct sub2_s {
    unit_stats stats[2];
    u32_t mask;
}
sub2;

#endif
