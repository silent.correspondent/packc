#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stddef.h>
#include <assert.h>
#include <string.h>

#include "sample.h"
#include "result.h"

void test_build_hello()
{
    const hello msg =
    {
        {'1', '2', '3', '4', '5', '6', '7'},
        0x3925
    };
    const uint8_t expected[] =
    {
        '1', '2', '3', '4', '5', '6', '7', 0x39, 0x25
    };
    uint8_t actual[sizeof(expected)] = {0};

    stream_t s;
    s.b = actual;
    s.sz = sizeof(actual);

    assert(pack_hello(&s, &msg));
    assert(memcmp(actual, expected, sizeof(expected)) == 0);
    assert(s.sz == 0);
}

void test_test32()
{
    const test32 msg =
    {
        0x11223344
    };
    const uint8_t expected[] =
    {
        0x11, 0x22, 0x33, 0x44
    };

    uint8_t actual[sizeof(expected)] = {0};
    test32 unpacked = {0};

    // Test the packing
    stream_t stream;

    stream.b = actual;
    stream.sz = sizeof(actual);

    assert (pack_test32(&stream, &msg));
    assert(memcmp(actual, expected, sizeof(expected)) == 0);
    assert(stream.sz == 0);

    // Test the unpacking
    stream.b = actual;
    stream.sz = sizeof(actual);

    assert (unpack_test32(&unpacked, &stream));
    assert(unpacked.value == msg.value);
    assert(stream.sz == 0);
}

void test_test16()
{
    stream_t istream;

    const test16 msg =
    {
        0x2334
    };
    const uint8_t expected[] =
    {
        0x23, 0x34
    };
    uint8_t actual[sizeof(expected)] = {0};
    test16 unpacked = {0};

    // Test the packing
    istream.b = actual;
    istream.sz = sizeof(actual);

    assert(pack_test16(&istream, &msg));
    assert(memcmp(actual, expected, sizeof(expected)) == 0);
    assert(istream.sz == 0);

    // Test the unpacking
    istream.b = actual;
    istream.sz = sizeof(actual);

    assert(unpack_test16(&unpacked, &istream));
    assert(unpacked.value == msg.value);
}

void test_sub2()
{
    stream_t istream;

    const sub2 msg =
    {
        {
           { 0x11, 0x22, 0x33},
           { 0x44, 0x55, 0x66}
        },
        0x778899aa
    };

    const uint8_t expected[] =
    {
        0x11, 0x22, 0x33,
        0x44, 0x55, 0x66,
        0x77, 0x88, 0x99, 0xaa
    };
    uint8_t actual[sizeof(expected)] = {0};
    sub2 unpack = {0};

    // Test the packing
    istream.b = actual;
    istream.sz = sizeof(actual);

    assert(pack_sub2(&istream, &msg));
    assert(memcmp(actual, expected, sizeof(expected)) == 0);
    assert(istream.sz == 0);

    // Test the unpacking
    istream.b = actual;
    istream.sz = sizeof(actual);

    assert(unpack_sub2(&unpack, &istream));
    assert(unpack.mask== msg.mask);
    assert(unpack.stats[0].dins == msg.stats[0].dins);
    assert(unpack.stats[1].douts == msg.stats[1].douts);
    assert(unpack.stats[2].ovf == msg.stats[2].ovf);
}

void test_gapfill()
{
    stream_t istream;
    const gapfill msg =
    {
        0x11,
        0x2233,
        0x44556677
    };
    const uint8_t expected[] =
    {
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77
    };
    uint8_t actual[sizeof(expected)] = {0};
    gapfill unpack = {0};

    // Test the packing
    istream.b = actual;
    istream.sz = sizeof(actual);

    assert(pack_gapfill(&istream, &msg));
    assert(memcmp(actual, expected, sizeof(expected)) == 0);
    assert(istream.sz == 0);

    // Test the unpacking
    istream.b = actual;
    istream.sz = sizeof(actual);

    assert(unpack_gapfill(&unpack, &istream));
    assert(istream.sz == 0);
    assert(unpack.v8 == msg.v8);
    assert(unpack.v16 == msg.v16);
    assert(unpack.v32 == msg.v32);
}

void test_funkyarrays()
{
    stream_t istream;
    const funkyarrays msg =
    {
        0x11,
        { 0x2222, 0x3333, 0x4444 },
        { 0x55555555, 0x66666666 }
    };
    const uint8_t expected[] =
    {
       0x11,
       0x22, 0x22,
       0x33, 0x33,
       0x44, 0x44,
       0x55, 0x55, 0x55, 0x55,
       0x66, 0x66, 0x66, 0x66
    };
    uint8_t actual[sizeof(expected)] = {0};
    funkyarrays unpack={0};

    // Test the packing
    istream.b = actual;
    istream.sz = sizeof(actual);

    assert(pack_funkyarrays(&istream, &msg));
    assert(memcmp(actual, expected, sizeof(expected)) == 0);
    assert(istream.sz == 0);

    // Test the unpacking
    istream.b = actual;
    istream.sz = sizeof(actual);

    assert(unpack_funkyarrays(&unpack, &istream));
    assert(unpack.pad == msg.pad);
    assert(unpack.a16[0] == msg.a16[0]);
    assert(unpack.a16[1] == msg.a16[1]);
    assert(unpack.a16[2] == msg.a16[2]);
    assert(unpack.a32[0] == msg.a32[0]);
    assert(unpack.a32[1] == msg.a32[1]);
}

int main()
{
    test_build_hello();
    test_test32();
    test_test16();
    test_gapfill();
    test_funkyarrays();
    test_sub2();

    return 0;
}

