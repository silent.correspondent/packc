#include <algorithm>
#include <iostream>
#include <fstream>
#include "node.h"

static const std::string header_guard(const char *target)
{
    std::string data(target);
    std::transform(data.begin(), data.end(), data.begin(), ::toupper);
    return data + "_H";
}

static bool is_file_name_char(char c)
{
    return (c == '.' || isalnum(c));
}

static const char *source_include(const char *source)
{
    const char *ret = source;
    const char *i;
    bool update_ret = true;

    for (i = source; *i; i++)
    {
        if (is_file_name_char(*i))
        {
            if (update_ret)
            {
                ret = i;
                update_ret = false;
            }
        }
        else
        {
            update_ret = true;
        }
    }

    return ret;
}

void generate_header(
        const char *source, const char *target,
        const Struct_list &structs)
{
    std::ofstream hfile;
    std::string fname(target);
    hfile.open (fname + ".h", std::ios::out);

    std::ostream &s = hfile;
    const std::string guard = header_guard(target);

    s << "#ifndef " << guard << std::endl;
    s << "#define " << guard << std::endl;
    s << std::endl;
    s << "#include \"" << source_include(source) << "\"" << std::endl;
    s << "#include \"pack.h\"" << std::endl;
    s << std::endl;

    for (const auto &n : structs)
    {
        n->generate_prototype(s);
    }

    s << "#endif // ifndef " << guard << std::endl;
}

void generate_source(const char *target, const Struct_list &structs)
{
    std::ofstream cfile;
    std::string fname(target);

    cfile.open (fname + ".c", std::ios::out);

    std::ostream &s = cfile;
    s << "#include \"" << fname << ".h\"" << std::endl;
    s << std::endl;

    for (const auto &n : structs)
    {
        n->generate_pack(s);
        n->generate_unpack(s);
    }
}


Typename::Typename(std::string *identifier, bool user_type)
: identifier_(identifier),
    user_type_(user_type)
{
}

const std::string &Typename::get_identifier() const
{
    return *identifier_;
}

const std::string Typename::member_read(const std::string &member_name) const
{
    if (user_type_)
    {
        return "&src->" + member_name;
    }
    else
    {
        return "src->" + member_name;
    }
}

const std::string Typename::member_write(const std::string &member_name) const
{
    return "&dst->" + member_name;
}

Field::Field(Typename *type, std::string *identifier)
: type_(type),
    identifier_(identifier)
{
}

bool Field::is_need_iterator() const
{
    return false;
}

static std::string indent(int level)
{
    std::string s;
    for (int i = 0; i < level; i++)
    {
        s += "  ";
    }

    return s;
}

void Field::generate_invoke_pack(std::ostream &s, int level) const
{
    s << indent(level) << "ret = pack_" << type_->get_identifier()
        << "(dst, " << type_->member_read(*identifier_) << ");" << std::endl;
}

void Field::generate_invoke_unpack(std::ostream &s, int level) const
{
    s << indent(level) << "ret = unpack_" << type_->get_identifier()
        << "(" << type_->member_write(*identifier_) << ", src);" << std::endl;
}

Array::Array(Typename *type, std::string *identifier, std::string *size)
: type_(type),
    identifier_(identifier),
    size_(size)
{
}

bool Array::is_need_iterator() const
{
    return true;
}

void Array::generate_invoke_pack(std::ostream &s, int level) const
{
    s << indent(level) << "for (i = 0; (ret == true) && (i < " << *size_ << "); ++i)" << std::endl;
    s << indent(level) << "{" << std::endl;
    s << indent(level + 1) << "ret = pack_" << type_->get_identifier()
        << "(dst, " << type_->member_read(*identifier_) << "[i]);" << std::endl;
    s << indent(level) << "}" << std::endl;
}

void Array::generate_invoke_unpack(std::ostream &s, int level) const
{
    s << indent(level) << "for (i = 0; (ret == true) && (i < " << *size_ << "); ++i)" << std::endl;
    s << indent(level) << "{" << std::endl;
    s << indent(level + 1) << "ret = unpack_" << type_->get_identifier()
        << "(" << type_->member_write(*identifier_) << "[i], src);" << std::endl;
    s << indent(level) << "}" << std::endl;
}

Struct::Struct(std::string *name, Node_list *members)
: name_(name),
    members_(members),
    need_iterator_(false)
{
    for (auto &m : *members)
    {
        if (m->is_need_iterator())
        {
            need_iterator_ = true;
            break;
        }
    }
}

void Struct::generate_pack_sig(std::ostream &s) const
{
    s << "bool pack_" << name_->data()
        << "(stream_t *dst, const " << name_->data() << " *src)";
}

void Struct::generate_unpack_sig(std::ostream &s) const
{
    s << "bool unpack_" << name_->data()
        << "(" << name_->data() << " *dst, stream_t *src)";
}

void Struct::generate_prototype(std::ostream &s) const
{
    generate_pack_sig(s);
    s << ";" << std::endl;
    generate_unpack_sig(s);
    s << ";" << std::endl;
    s << std::endl;
}

void Struct::generate_common(std::ostream &s,
        std::function<void (std::ostream &, const Node &, int)> gen_invoke) const
{
    s << std::endl;
    s << "{" << std::endl;

    if (need_iterator_ == true)
    {
        s << indent(1) << "size_t i;" << std::endl;
    }

    s << "  bool ret = true;" << std::endl;
    for (const auto &m : *members_)
    {
        if (&m == &members_->front())
        {
            gen_invoke(s, *m, 1);
        }
        else
        {
            s << indent(1) << "if (ret == true)" << std::endl;
            s << indent(1) << "{" << std::endl;
            gen_invoke(s, *m, 2);
            s << indent(1) << "}" << std::endl;
        }
    }
    s << indent(1) << "return ret;" << std::endl;
    s << "}" << std::endl << std::endl;
}

void Struct::generate_pack(std::ostream &s) const
{
    generate_pack_sig(s);
    generate_common(s,[](std::ostream &s, const Node &node, int level)
            {
                node.generate_invoke_pack(s, level);
            });
}

void Struct::generate_unpack(std::ostream &s) const
{
    generate_unpack_sig(s);
    generate_common(s,[](std::ostream &s, const Node &node, int level)
            {
                node.generate_invoke_unpack(s, level);
            });
}
