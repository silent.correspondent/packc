#ifndef NODE_H
#define NODE_H

#include <string>
#include <memory>
#include <iostream>
#include <vector>

class Node
{
    public:
        virtual ~Node() {}
        virtual void generate_invoke_pack(std::ostream &s, int indentation) const = 0;
        virtual void generate_invoke_unpack(std::ostream &s, int indentation) const = 0;
        virtual bool is_need_iterator() const = 0;
};

typedef std::unique_ptr<Node>   Node_ptr;
typedef std::vector<Node_ptr>   Node_list;

class Typename
{
    public:
        Typename(std::string *identifier, bool user_type);
        const std::string &get_identifier() const;
        const std::string member_read(const std::string &member_name) const;
        const std::string member_write(const std::string &member_name) const;

    private:
        std::unique_ptr<std::string> identifier_;
        bool user_type_;
};

class Field : public Node
{
    public:
        Field(Typename *type, std::string *identifier);
        virtual void generate_invoke_pack(std::ostream &s, int indentation) const override;
        virtual void generate_invoke_unpack(std::ostream &s, int indentation) const override;
        virtual bool is_need_iterator() const override;

    private:
        std::unique_ptr<Typename> type_;
        std::unique_ptr<std::string> identifier_;
};

class Array : public Node
{
    public:
        Array(Typename *type, std::string *identifier, std::string *size);
        virtual void generate_invoke_pack(std::ostream &s, int indentation) const override;
        virtual void generate_invoke_unpack(std::ostream &s, int indentation) const override;
        virtual bool is_need_iterator() const override;

    private:
        std::unique_ptr<Typename> type_;
        std::unique_ptr<std::string> identifier_;
        std::unique_ptr<std::string> size_;
};

class Struct
{
    public:
        Struct(std::string *name, Node_list *members);
        void generate_prototype(std::ostream &s) const;
        void generate_pack(std::ostream &s) const;
        void generate_unpack(std::ostream &s) const;

    private:
        std::unique_ptr<std::string> name_;
        std::unique_ptr<Node_list> members_;
        bool need_iterator_;

        void generate_pack_sig(std::ostream &s) const;
        void generate_unpack_sig(std::ostream &s) const;
        void generate_common(std::ostream &,
                std::function<void (std::ostream &, const Node &, int)>) const;
};

typedef std::unique_ptr<Struct> Struct_ptr;
typedef std::vector<Struct_ptr> Struct_list;

void generate_header(
        const char *source, const char *target,
        const Struct_list &structs);

void generate_source(
        const char *target,
        const Struct_list &structs);

#endif
