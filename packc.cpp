#include <iostream>
#include <stdio.h>
#include <boost/program_options.hpp>
#include "packc.h"

namespace po = boost::program_options;

int main(int argc, char *argv[])
{
    const char *output = "packc_out";

    po::options_description desc("Usage: <input> [-o|--output output]");
    desc.add_options()
        ("help", "produce help message")
        ("input", po::value<std::string>(), "Input file name")
        ("output", po::value<std::string>(), "Output file name")
        ;

    po::positional_options_description posopts;
    posopts.add("input", 1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(posopts).run(), vm);
    po::notify(vm);

    if (vm.count("help") || vm.count("input") == 0)
    {
        std::cout << desc << std::endl;
        return 1;
    }

    if (vm.count("output"))
    {
        output = vm["output"].as<std::string>().c_str();
    }

    const char *input = vm["input"].as<std::string>().c_str();
    return compile(input, output);
}
